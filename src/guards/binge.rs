use chrono::{DateTime, NaiveDateTime, Utc};
use log::info;

use crate::core::Chamomile;
use crate::dinger::Ding;
use crate::statistic::Statistic;
use crate::utils::progress::Progress;
use crate::utils::timer::Timer;

const DATE_FORMAT: &str = "%F %X";

pub struct BingeGuard;

impl BingeGuard {
    pub fn analyze(core: &mut Chamomile) {
        info!("Running binge analyzer...");
        let mut dings = 0;
        let mut timer = Timer::new();
        let mut progress = Progress::new(core.stats.len() as u64);
        let mut stats = core.stats.clone();
        while !stats.is_empty() {
            progress.add(1);
            let item = stats.get(0).unwrap().clone();
            stats.remove(0);
            let span = Self::get_span(core, &stats, &item);
            let spanning = if let Some(first) = span.first() {
                if let Some(last) = span.last() {
                    (first.command.execution.timestamp - last.command.execution.timestamp)
                        >= (core.cfg.guards.binge.period_seconds() * 0.98_f64)
                } else {
                    false
                }
            } else {
                false
            };
            if !span.is_empty() && spanning {
                let mut binged = true;
                let mut last_item: Option<Statistic> = None;
                for stat in &span {
                    if let Some(li) = last_item {
                        let item_stamp = li.command.execution.timestamp;
                        let stat_stamp = stat.command.execution.timestamp;
                        let diff = item_stamp - stat_stamp;
                        if diff > core.cfg.guards.binge.interval_seconds() {
                            binged = false;
                            break;
                        } else {
                            last_item = Some(stat.clone());
                        }
                    } else {
                        last_item = Some(stat.clone());
                    }
                }
                if binged {
                    dings += 1;
                    Self::ding(core, span);
                }
            }
            if progress.ready() {
                info!("{}", progress.draw());
            }
        }
        timer.stop();
        info!(
            "Binge analyzer finished in {:.3}s with {} dings.",
            timer.diff(),
            dings
        );
    }

    fn get_span(core: &Chamomile, stats: &Vec<Statistic>, item: &Statistic) -> Vec<Statistic> {
        let mut items = Vec::new();
        for stat in stats {
            if stat.command.execution.timestamp < item.command.execution.timestamp {
                let diff = item.command.execution.timestamp - stat.command.execution.timestamp;
                if diff <= core.cfg.guards.binge.period_seconds() {
                    items.push(stat.clone());
                } else {
                    break;
                }
            }
        }
        items
    }

    fn ding(core: &mut Chamomile, span: Vec<Statistic>) {
        let first = span.first().unwrap();
        let last = span.last().unwrap();
        let first_dt = DateTime::<Utc>::from_utc(
            NaiveDateTime::from_timestamp(first.command.execution.timestamp as i64, 0),
            Utc,
        );
        let last_dt = DateTime::<Utc>::from_utc(
            NaiveDateTime::from_timestamp(last.command.execution.timestamp as i64, 0),
            Utc,
        );
        let elapsed = first.command.execution.timestamp - last.command.execution.timestamp;
        let average = elapsed / (span.len() as f64);
        let first_str = first_dt.format(DATE_FORMAT);
        let last_str = last_dt.format(DATE_FORMAT);
        let desc = format!(
            "FROM: {} | TO: {} | AVG: {:.3}s | COUNT: {}",
            last_str,
            first_str,
            average,
            span.len()
        );
        core.dinger
            .add(Ding::new("binge", core.cfg.guards.binge.value, desc))
    }
}
