use log::info;

use crate::core::Chamomile;
use crate::dinger::Ding;
use crate::statistic::Statistic;
use crate::utils::progress::Progress;
use crate::utils::timer::Timer;

pub struct BurstGuard;

impl BurstGuard {
    pub fn analyze(core: &mut Chamomile) {
        info!("Running burst analyzer...");
        let mut dings = 0;
        let mut timer = Timer::new();
        let mut progress = Progress::new(core.stats.len() as u64);
        let mut ix_a = 0u64;
        let mut ix_b = core.cfg.guards.burst.count;
        while ix_b < core.stats.len() as u64 {
            progress.add(1);
            let mut items = Vec::new();
            for ix in ix_a..ix_b {
                let stat: Option<&Statistic> = core.stats.get(ix as usize);
                if let Some(stat) = stat {
                    items.push(stat.clone());
                }
            }
            let mut sum = 0f64;
            let mut previous: Option<Statistic> = None;
            for item in &items {
                if let Some(prev) = previous.clone() {
                    let istamp = item.command.execution.timestamp;
                    let pstamp = prev.command.execution.timestamp;
                    sum += pstamp - istamp;
                } else {
                    previous = Some(item.clone());
                }
            }
            let avg = sum / (items.len() - 1) as f64;
            if avg < core.cfg.guards.burst.interval_seconds() && avg != 0_f64 {
                dings += 1;
                Self::ding(core, &items, avg);
            }
            ix_a += 1;
            ix_b += 1;
            if progress.ready() {
                info!("{}", progress.draw());
            }
        }
        timer.stop();
        info!(
            "Burst analyzer finished in {:.3}s with {} dings.",
            timer.diff(),
            dings
        );
    }

    fn ding(core: &mut Chamomile, items: &Vec<Statistic>, avg: f64) {
        let mut cmd_names = Vec::new();
        let mut cmd_stamps = Vec::new();
        for item in items {
            cmd_names.push(item.command.name.clone());
            cmd_stamps.push(item.command.execution.timestamp.to_string())
        }
        let desc = format!(
            "AVG: {} | CMDS: {} | TIMES: {}",
            avg,
            cmd_names.join("/"),
            cmd_stamps.join("/")
        );
        core.dinger
            .add(Ding::new("burst", core.cfg.guards.burst.value, desc))
    }
}
