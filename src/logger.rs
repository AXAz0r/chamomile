use log::info;
use log4rs::{
    append::console::ConsoleAppender,
    config::{Appender, Logger as LFRS, Root},
    encode::pattern::PatternEncoder,
    Config,
};

static LOG_PATTERN: &str = "{d(%Y-%m-%d %H:%M:%S)} | {l} | {t} | {m}{n}";

pub struct Logger;

impl Logger {
    pub fn init() -> anyhow::Result<(), anyhow::Error> {
        let stdout = ConsoleAppender::builder()
            .encoder(Box::new(PatternEncoder::new(LOG_PATTERN)))
            .build();
        let config = Config::builder()
            .appender(Appender::builder().build("stdout", Box::new(stdout)))
            .logger(LFRS::builder().build("chamomile", log::LevelFilter::Info))
            .build(
                Root::builder()
                    .appender("stdout")
                    .build(log::LevelFilter::Info),
            )?;
        let _ = log4rs::init_config(config)?;
        info!("Logger initialized!");
        Ok(())
    }
}
