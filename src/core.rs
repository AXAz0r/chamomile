use crate::config::Config;
use crate::dinger::Dinger;
use crate::guards::binge::BingeGuard;
use crate::guards::burst::BurstGuard;
use crate::statistic::Statistic;
use crate::utils::timer::Timer;
use log::info;
use mongodb::options::FindOptions;
use tokio::stream::StreamExt;

pub struct Chamomile {
    pub cfg: Config,
    pub db: mongodb::Client,
    pub stats: Vec<Statistic>,
    pub dinger: Dinger,
}

impl Chamomile {
    pub async fn new() -> anyhow::Result<Self, anyhow::Error> {
        info!("Creating Chamomile instance.");
        let mut cfg = Config::new()?;
        cfg.arg_override();
        cfg.describe();
        let db = mongodb::Client::with_uri_str(&cfg.database.get_uri()).await?;
        Ok(Self {
            cfg,
            db,
            stats: Vec::new(),
            dinger: Dinger::new(),
        })
    }

    async fn collect(&mut self) -> anyhow::Result<(), anyhow::Error> {
        info!("Collecting all detailed command documents...");
        let mut timer = Timer::new();
        let lookup = self.cfg.target.get_lookup();
        let options = FindOptions::builder().build();
        let mut cursor = self
            .db
            .database(&self.cfg.database.name)
            .collection("DetailedCommandStats")
            .find(lookup, options)
            .await?;
        while let Some(doc_result) = cursor.next().await {
            if let Ok(doc) = doc_result {
                let statistic = bson::from_document::<Statistic>(doc)?;
                self.stats.push(statistic);
            }
        }
        timer.stop();
        info!(
            "Collected {} documents in {:.3}s.",
            self.stats.len(),
            timer.diff()
        );
        self.sort();
        Ok(())
    }

    fn sort(&mut self) {
        info!("Sorting all documents...");
        let mut timer = Timer::new();
        self.stats.dedup_by(|a, b| a.message.id == b.message.id);
        self.stats.sort_by(|a, b| {
            a.command
                .execution
                .timestamp
                .partial_cmp(&b.command.execution.timestamp)
                .unwrap()
        });
        self.stats.reverse();
        timer.stop();
        info!("Sorted the documents in {:.3}s.", timer.diff());
    }

    pub fn analyze(&mut self) {
        BurstGuard::analyze(self);
        BingeGuard::analyze(self);
        info!("Total Dings: {}", self.dinger.dings.len());
        let _ = self.dinger.describe(true);
        info!("Total Ding Value: {}", self.dinger.total());
    }

    pub async fn run(&mut self) -> anyhow::Result<(), anyhow::Error> {
        info!("Chamomile is starting.");
        self.collect().await?;
        if self.stats.is_empty() {
            info!("No stats were found for the configured lookup.");
        } else {
            self.analyze();
            self.dinger.dump(&self.cfg)?;
        }
        Ok(())
    }
}
