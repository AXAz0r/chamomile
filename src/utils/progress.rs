use crate::utils::timer::Timer;

const MIN_DIFF: u64 = 10;

const BAR_WIDTH: u64 = 40;

const BAR_FULL: &str = "=";
const BAR_EMPTY: &str = " ";

pub struct Progress {
    current: u64,
    total: u64,
    stamp: u64,
    timer: Timer,
}

impl Progress {
    pub fn new(total: u64) -> Self {
        Self {
            current: 0,
            total,
            stamp: chrono::Utc::now().timestamp() as u64,
            timer: Timer::new(),
        }
    }

    pub fn add(&mut self, amt: u64) {
        self.current += amt;
    }

    pub fn ready(&mut self) -> bool {
        let now = chrono::Utc::now().timestamp() as u64;
        if now >= self.stamp + MIN_DIFF {
            self.stamp = now;
            true
        } else {
            self.current == self.total
        }
    }

    pub fn draw(&mut self) -> String {
        let perc = self.current as f64 / self.total as f64;
        let full_len = (perc * BAR_WIDTH as f64) as u64;
        let empty_len = BAR_WIDTH - full_len;
        let full_str = BAR_FULL.to_string().repeat(full_len as usize);
        let empty_str = BAR_EMPTY.to_string().repeat(empty_len as usize);
        self.timer.stop();
        format!(
            "PROGRESS: [{}{}] {:.3}% {}/{} | Elapsed: {:.3}s",
            full_str,
            empty_str,
            perc * 100_f64,
            self.current,
            self.total,
            self.timer.diff()
        )
    }
}
