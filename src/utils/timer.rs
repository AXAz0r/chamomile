pub struct Timer {
    start: f64,
    finish: f64,
}

impl Timer {
    fn now() -> f64 {
        chrono::Utc::now().timestamp_millis() as f64 / 1_000_f64
    }

    pub fn new() -> Self {
        Self {
            start: Self::now(),
            finish: 0_f64,
        }
    }

    pub fn stop(&mut self) {
        self.finish = Self::now();
    }

    pub fn diff(&self) -> f64 {
        self.finish - self.start
    }
}
