use crate::core::Chamomile;
use crate::logger::Logger;

pub mod config;
pub mod core;
pub mod dinger;
pub mod guards;
pub mod logger;
pub mod statistic;
pub mod utils;

#[tokio::main]
async fn main() -> anyhow::Result<(), anyhow::Error> {
    Logger::init()?;
    Chamomile::new().await?.run().await?;
    Ok(())
}
