use crate::config::Config;
use log::info;
use std::collections::HashMap;

const SEPARATOR_WIDTH: usize = 50;

#[derive(Clone)]
pub struct Ding {
    pub source: String,
    pub value: u64,
    pub description: String,
}

impl Ding {
    pub fn new(src: impl ToString, val: u64, desc: impl ToString) -> Self {
        Self {
            source: src.to_string(),
            value: val,
            description: desc.to_string(),
        }
    }
}

#[derive(Default)]
pub struct Dinger {
    pub dings: Vec<Ding>,
}

impl Dinger {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn ding_hashmap(&self) -> HashMap<String, u64> {
        let mut data = HashMap::new();
        for ding in &self.dings {
            let key = ding.source.clone();
            let mut val = data.get(&key).unwrap_or_else(|| &0).clone();
            val += 1;
            data.insert(key, val);
        }
        data
    }

    pub fn add(&mut self, ding: Ding) {
        self.dings.push(ding);
    }

    pub fn total(&self) -> u64 {
        let mut sum = 0;
        for ding in &self.dings {
            sum += ding.value;
        }
        sum
    }

    pub fn describe(&self, print: bool) -> Vec<String> {
        let mut lines = Vec::new();
        let map = self.ding_hashmap();
        if map.is_empty() {
            lines.push("No dings were activated for this query.".to_string());
        } else {
            for key in map.keys() {
                if let Some(val) = map.get(key) {
                    let line = format!("{} Guard Dings: {}", key.to_uppercase(), val);
                    if print {
                        info!("{}", &line);
                    }
                    lines.push(line);
                }
            }
        }
        lines
    }

    pub fn dump(&self, cfg: &Config) -> anyhow::Result<(), anyhow::Error> {
        if !std::path::Path::new("logs/").exists() {
            std::fs::create_dir("logs")?;
        }
        let mut lines = Vec::<String>::new();
        lines.push("Guard Reports".to_string());
        lines.push(cfg.target.describe(false).join(" | "));
        if !self.dings.is_empty() {
            lines.push("=".to_string().repeat(SEPARATOR_WIDTH));
            for ding in &self.dings {
                let line = format!(
                    "GUARD: {} | VAL: {} | {}",
                    ding.source.to_uppercase(),
                    ding.value,
                    ding.description
                );
                lines.push(line);
            }
        }
        lines.push("=".to_string().repeat(SEPARATOR_WIDTH));
        lines.append(&mut self.describe(false));
        lines.push("=".to_string().repeat(SEPARATOR_WIDTH));
        lines.push(format!("Total Ding Value: {}", self.total()));
        let path_str = format!("logs/{}.log", cfg.target.dump_name());
        let out_path = std::path::Path::new(&path_str);
        std::fs::write(out_path, lines.join("\n"))?;
        Ok(())
    }
}
