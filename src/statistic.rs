use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize)]
pub struct Execution {
    pub time: f64,
    pub timestamp: f64,
    pub formatted: String,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Command {
    pub name: String,
    pub category: String,
    pub nsfw: bool,
    pub execution: Execution,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Message {
    pub id: i64,
    pub content: String,
    pub arguments: Vec<String>,
    pub created_at: String,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct User {
    pub id: i64,
    pub name: String,
    pub discriminator: String,
    pub display_name: String,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Channel {
    pub id: Option<i64>,
    pub name: Option<String>,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Guild {
    pub id: Option<i64>,
    pub name: Option<String>,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Statistic {
    pub command: Command,
    pub message: Message,
    pub user: User,
    pub channel: Channel,
    pub guild: Guild,
}
