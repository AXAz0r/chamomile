use std::fs::File;
use std::io::BufReader;

use bson::{doc, Document};
use log::info;
use serde::{Deserialize, Serialize};

#[derive(Clone, Default, Deserialize, Serialize)]
pub struct TargetConfig {
    pub user: u64,
    pub category: Option<String>,
    pub command: Option<String>,
}

impl TargetConfig {
    pub fn get_lookup(&self) -> Document {
        if self.category.is_some() {
            let cat = self.category.clone().unwrap_or_else(|| "".to_string());
            if self.command.is_some() {
                let cmd = self.command.clone().unwrap_or_else(|| "".to_string());
                doc! {"user.id": self.user, "command.name": cmd, "command.category": cat}
            } else {
                doc! {"user.id": self.user, "command.category": cat}
            }
        } else {
            if self.command.is_some() {
                let cmd = self.command.clone().unwrap_or_else(|| "".to_string());
                doc! {"user.id": self.user, "command.name": cmd}
            } else {
                doc! {"user.id": self.user}
            }
        }
    }

    pub fn dump_name(&self) -> String {
        format!(
            "{}_{}_{}",
            self.user,
            self.category.clone().unwrap_or_else(|| "nomdl".to_string()),
            self.command.clone().unwrap_or_else(|| "nocmd".to_string())
        )
    }

    pub fn describe(&self, print: bool) -> Vec<String> {
        let mut lines = Vec::<String>::new();
        lines.push(format!("User: {}", self.user));
        lines.push(format!(
            "Category: {}",
            if let Some(cat) = &self.category {
                cat
            } else {
                "None"
            }
        ));
        lines.push(format!(
            "Command: {}",
            if let Some(cmd) = &self.command {
                cmd
            } else {
                "None"
            }
        ));
        if print {
            for line in &lines {
                info!("{}", line);
            }
        }
        lines
    }
}

#[derive(Clone, Deserialize, Serialize)]
pub struct DatabaseConfig {
    pub host: String,
    pub port: u16,
    pub auth: bool,
    pub user: Option<String>,
    pub pass: Option<String>,
    pub name: String,
}

impl Default for DatabaseConfig {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".to_string(),
            port: 27017,
            auth: false,
            user: None,
            pass: None,
            name: "sigma".to_string(),
        }
    }
}

impl DatabaseConfig {
    pub fn get_uri(&self) -> String {
        if self.auth && self.user.is_some() && self.pass.is_some() {
            format!(
                "mongodb://{}:{}@{}:{}/admin",
                &self.user.clone().unwrap_or_else(|| "".to_string()),
                &self.pass.clone().unwrap_or_else(|| "".to_string()),
                &self.host,
                &self.port
            )
        } else {
            format!("mongodb://{}:{}/admin", &self.host, &self.port)
        }
    }

    pub fn describe(&self) {
        if self.auth {
            info!(
                "URI: mongodb://[redacted]:[redacted]@{}:{}/admin",
                &self.host, &self.port
            )
        } else {
            info!("URI: {}", self.get_uri())
        }
        info!("Database: {}", self.name);
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
pub struct GuardConfig {
    #[serde(default = "GuardConfig::default_value")]
    pub count: u64,
    #[serde(default = "GuardConfig::default_value")]
    pub value: u64,
    #[serde(default = "GuardConfig::default_value")]
    pub interval: u64,
    #[serde(default = "GuardConfig::default_value")]
    pub period: u64,
}

impl GuardConfig {
    pub fn default_value() -> u64 {
        0
    }

    pub fn describe(&self) -> String {
        format!(
            "Count: {} | Value: {} | Interval: {} | Period: {}",
            self.count, self.value, self.interval, self.period
        )
    }

    fn seconds_from_millis(mils: u64) -> f64 {
        mils as f64 / 1_000_f64
    }

    pub fn interval_seconds(&self) -> f64 {
        Self::seconds_from_millis(self.interval)
    }

    pub fn period_seconds(&self) -> f64 {
        Self::seconds_from_millis(self.period)
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
pub struct Guards {
    pub pattern: GuardConfig,
    pub expectation: GuardConfig,
    pub burst: GuardConfig,
    pub binge: GuardConfig,
    pub interval: GuardConfig,
}

impl Guards {
    pub fn describe(&self) {
        info!("Pattern: {}", self.pattern.describe());
        info!("Expectation: {}", self.expectation.describe());
        info!("Burst: {}", self.burst.describe());
        info!("Binge: {}", self.binge.describe());
        info!("Interval: {}", self.interval.describe());
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
pub struct Config {
    pub target: TargetConfig,
    pub database: DatabaseConfig,
    pub guards: Guards,
}

impl Config {
    pub fn new() -> anyhow::Result<Self, anyhow::Error> {
        let cfg_path = std::path::Path::new("config.yml");
        if cfg_path.exists() {
            let file = File::open("config.yml")?;
            let reader = BufReader::new(file);
            Ok(serde_yaml::from_reader(reader)?)
        } else {
            Ok(Self::default())
        }
    }

    pub fn arg_override(&mut self) {
        let mut args: Vec<_> = std::env::args_os().collect();
        args.remove(0);
        if let Some(uid_os_str) = args.pop() {
            if let Some(uid_str) = uid_os_str.to_str() {
                if let Ok(uid) = uid_str.parse::<u64>() {
                    info!("User ID overridden to {}!", uid);
                    self.target.user = uid;
                }
            }
        }
    }

    pub fn describe(&self) {
        let _ = self.target.describe(true);
        self.database.describe();
        self.guards.describe();
    }
}
